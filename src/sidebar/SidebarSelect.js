import './sidebarSelect.css';


const SidebarSelect = (props) => {
    return (
        <select name="" id="sidebar__Select" className={props.color}>
        <option disabled selected>{props.content}</option>
    </select>
    )
}

export default SidebarSelect;