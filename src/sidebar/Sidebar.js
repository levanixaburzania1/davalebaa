import './sidebar.css';
import SidebarSelect from './SidebarSelect';

const Sidebar = () => {
    return (
        <sidebar class="sidebar">
        <SidebarSelect content={"Choose Niche"} color={"sidebar__Select--dark"}/>
        <SidebarSelect content= {"Choose Category"} color={"sidebar__Select--light"}/>
    </sidebar>
    )
}

export default Sidebar;