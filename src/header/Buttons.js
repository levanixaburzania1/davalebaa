import './button.css';

const Button = ({content}) => {
    return (
        <button className="header__btn">{content}</button>
    )
}

export default Button;