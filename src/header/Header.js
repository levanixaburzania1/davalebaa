import './header.css';
import Button from './Buttons';
import Icon from './Icon.js'


const Header = () => {
    return (
        <header class="header">
        <div class="header__raw1">
        <div class="header__raw1-left">
        <Button content={"Select ALL"}/>
        <span class="seperate"></span>
        <span class="header__selected_items">selected <span id="selected_quantity">2</span> out of 50 products</span>
    </div>
    <div class="header__raw1-right">
        <nav class="nav">
            <input type="text" id="search-input" placeholder="search..."/>
            <button id="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </nav>
        <Button content={"Add to Inventory"}/>
        <button class="header__help-btn">
            <Icon nutsa="0" />
        </button>
    </div>
</div>
    <div class="header__raw2">
        <div class="header__sortby"> Sort by: </div>
        <select id="sort" class="header__sort">
            
            <option>asc</option>
            <option>desc</option>
        </select>
    </div>
    </header>
    )
}

export default Header;