import logo from "./logo.svg";
import "./App.css";
import Header from "./header/Header";
import Sidebar from "./sidebar/Sidebar";


function App() {
  return (
    <div className="App">
      <div className="content">
        <body>
          
          <Header />
          <Sidebar />
          
          <main class="main"></main>
        </body>
      </div>
    </div>
  );
}

export default App;
